<?
require_once __DIR__.'/config.req.php';

class Vodstream {
	public static function provide () {
		$GLOBALS['ctrl_name'] = "vodstream";
		$GLOBALS['json_object'] = json_decode(file_get_contents("php://input"), true);
		
		## validate post json
		if (Variable::validate()) {
			require_once __DIR__."/Vodstream.req.php";
			$qstring = $GLOBALS['config']->getrsaqstring();
			$livemaping = $GLOBALS['config']->generatePlaylist();
			$is_livemaping = !is_bool($livemaping);
			$lb = new NewCDNZoneBalancer();
			$cdn = $lb->findCDNByIP();
			$is_cdn = ($cdn['result_code'] == 200);

			switch($GLOBALS['json_object']['drm']) {
				case "aes":
					if ($is_livemaping && $is_cdn) {
						$return['result_code'] = 200;
						$return['result'] = "https://". $cdn['result'] ."/". $livemaping ."/playlist.m3u8?". $qstring;
						Logger::writelog($return);
					}
					if (!$is_livemaping) {
						$return['result_code'] = 430;
						$return['result'] = "Cannot find playlist.";
						newrelic_notice_error(serialize($return));
						Logger::writelog($return);
					}
					if (!$is_cdn) {
						$return['result_code'] = 420;
						// $return['result_code'] = $cdn['result_code'];
						$return['result'] = "Cannot find streaming server.";
						newrelic_notice_error(serialize($cdn));
						Logger::writelog($cdn);
					}
					$return['version'] = "3.0";
				break;
				case "wv":
					if ($is_livemaping && $is_cdn) {
						$return['result_code'] = 200;
						$return['streamurl'] = "https://". $cdn['result'] ."/". $livemaping ."/manifest.mpd?appid={$GLOBALS['json_object']['appid']}&type={$GLOBALS['json_object']['type']}&visitor={$GLOBALS['json_object']['visitor']}&uid={$GLOBALS['json_object']['uid']}";
						$return['license'] = "https://kd.stm.trueid.net/charybdis/drmdecrypt?". $qstring;
						// Logger::writelog($return);
					}
					if (!$is_livemaping) {
						$return['result_code'] = 430;
						$return['result'] = "Cannot find playlist.";
						newrelic_notice_error(serialize($return));
						Logger::writelog($return);
					}
					if (!$is_cdn) {
						$return['result_code'] = 420;
						// $return['result_code'] = $cdn['result_code'];
						$return['result'] = "Cannot find streaming server.";
						newrelic_notice_error(serialize($cdn));
						Logger::writelog($cdn);
					}
					$return['version'] = "3.0";
				break;
				case "fp":
					if ($is_livemaping && $is_cdn) {
						$return['result_code'] = 200;
						$return['streamurl'] = "https://". $cdn['result'] ."/". $livemaping ."/playlist.m3u8?appid={$GLOBALS['json_object']['appid']}&type={$GLOBALS['json_object']['type']}&visitor={$GLOBALS['json_object']['visitor']}&uid={$GLOBALS['json_object']['uid']}";
						$return['license'] = "https://kd.stm.trueid.net/scylla/drmdecrypt?". $qstring;
						// Logger::writelog($return);
					}
					if (!$is_livemaping) {
						$return['result_code'] = 430;
						$return['result'] = "Cannot find playlist.";
						newrelic_notice_error(serialize($return));
						Logger::writelog($return);
					}
					if (!$is_cdn) {
						$return['result_code'] = 420;
						// $return['result_code'] = $cdn['result_code'];
						$return['result'] = "Cannot find streaming server.";
						newrelic_notice_error(serialize($cdn));
						Logger::writelog($cdn);
					}
					$return['version'] = "3.0";
				break;
				default:
					$return = array('result_code' => 630, 'result' => "Invalid DRM request.");
					Logger::writelog($return);
				break;
			}
		}
		else {
			$return = array('result_code' => 600, 'result' => "Invalid request.");
			Logger::writelog($return);
		}
		
		// if(strpos($GLOBALS['json_object']->appid, "v2") <> false) $return['version'] = "2.0";
		return $return;
    }
}