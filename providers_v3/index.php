<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response; 

require_once __DIR__ .'/vendor/autoload.php';

$app = new \Slim\App;
$app->get('/version', function (Request $request, Response $response) {
	$response->withoutHeader('Allow');
	$return = array("response" => 200, "version" => "3.2.8", "release date" => "September 11, 2019", "powered by" => "Streaming application, TDMP.", "B64" => "bmV3cmVsaWMgYWdlbnQgZXJyb3IgbWVzc2FnZS4=", "checksum" => hash_file("sha256", dirname(__DIR__)."/providerschecksum.log"));
	$response->withJson($return);
	header("Content-Type: application/json");
	return $response;
});

$app->post('/streamingprovider', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Streamingprovider.php';
	// $response->withoutHeader('Allow');
	$return = Streamingprovider::provide($request, $response);
	// $response->withJson($return);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	// header("Access-Control-Allow-Method: POST,GET,OPTIONS");
	header("Access-Control-Allow-Method: *");
	// echo __DIR__ .'/logs/result_'. date("Ymd_h") .".log";
	// file_put_contents(__DIR__ .'/logs/result_'. date("Ymd_H") .".log",  serialize(json_decode($request->getBody(), true)) ." >> ". serialize($return) ."\n", FILE_APPEND);
	print(json_encode($return));
	exit;
	// return $response;
});

$app->post('/realtimestreamprovider', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Realtimestreamprovider.php';
	$return = Realtimestreamprovider::provide($request, $response);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Method: *");
	print(json_encode($return));
	exit;
	// return $response;
});

$app->post('/radiostreamprovider', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Radiostreamprovider.php';
	$return = Radiostreamprovider::provide($request, $response);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Method: *");
	print(json_encode($return));
	exit;
	// return $response;
});

$app->post('/securestreamprovider', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Securestreamprovider.php';
	$return = Securestreamprovider::provide($request, $response);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Method: *");
	print(json_encode($return));
	exit;
});

$app->post('/vodstreamprovider', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Vodstreamprovider.php';
	// $response->withoutHeader('Allow');
	$return = Vodstreamprovider::provide($request, $response);
	// $response->withJson($return);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	// echo __DIR__ .'/logs/result_'. date("Ymd_h") .".log";
	// file_put_contents(__DIR__ .'/logs/result_'. date("Ymd_H") .".log",  serialize(json_decode($request->getBody(), true)) ." >> ". serialize($return) ."\n", FILE_APPEND);
	print(json_encode($return));
	exit;
	// return $response;
});

$app->post('/tvodstreamprovider', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Tvodstreamprovider.php';
	$return = Tvodstreamprovider::provide($request, $response);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	print(json_encode($return));
	// var_dump($return);
	exit;
	// return $response;
});

$app->post('/eventstreamprovider', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Eventstreamprovider.php';
	$return = Eventstreamprovider::provide($request, $response);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	print(json_encode($return));
	exit;
});

$app->post('/eventstreamprovider_test', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Eventstreamprovider_test.php';
	$return = Eventstreamprovider::provide($request, $response);
	// header("Content-Type: application/json");
	// // header("Access-Control-Allow-Origin: *");
	// print(json_encode($return));
	exit;
});

########## test ###############

$app->post('/streamingprovider_test', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Streamingprovider_test.php';
	$return = Streamingprovider::provide($request, $response);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	print(json_encode($return));
	exit;
});
$app->get('/getlicense', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/libraries/Opensslcryption.php';
	$toencrypt = rand(0, 1000) ."-". time() ."-". $_GET['appid'];
	echo $toencrypt;
	echo "<br >";
	$secure_arr = json_decode(file_get_contents("/www/webapps/providers_v3/spmiddleware/assets/securestreamprovider/". $_GET['appid'] .".secure"), true);
	var_dump($secure_arr);
	echo "<br >";
	$return = Opensslcryption::encryptstr($toencrypt, $secure_arr['key'], $secure_arr['vector']);
	print(str_replace("=", "", base64_encode($return)));
	exit;
});

$app->post('/v3/livestream', function (Request $request, Response $response) {
	require_once __DIR__ .'/spapi/Livestream.php';
	$return = Livestream::provide();
	header("Content-Type: application/json");
	print(json_encode($return));
	exit;
});

$app->post('/v3/livestreamstg', function () {
	require_once __DIR__ .'/spapi/Livestreamstg.php';
	$return = Livestreamstg::provide();
	header("Content-Type: application/json");
	print(json_encode($return));
	exit;
});

$app->post('/v3/vodstream', function (Request $request, Response $response) {
	require_once __DIR__ .'/spapi/Vodstream.php';
	$return = Vodstream::provide();
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	print(json_encode($return));
	exit;
});

$app->run();
