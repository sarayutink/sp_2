<?
class Streamcontrol {	
	/**
		$stream_input = array (
			'uid' => string,
			'sessionid' => string,
			'appid' => string,
			'channelid' => string,
			'langid' => string,
			'stid' => string,
			'type' => string,
			'stme' => INT,
			'duration' => INT,
			'csip' => string,
			'geoblock' => string,
			'gps' => string,
			'agent' => string
		);
	*/
	
	public static function createStreamPath() {		
		$plurl = $GLOBALS['bizconf']->generatePlaylist();
		if (!empty($plurl)) {
			$qstring = "";
			if (strpos($plurl, "bk") === false) $qstring = $GLOBALS['bizconf']->getrsaqstring();
			else $qstring = $GLOBALS['bizconf']->getrsaqstring(true);
			
			return "$plurl?$qstring";
		}
		else {
			Logger::writelog(array('result_code' => 431, 'result' => 'LIVE_MAPPING_CONFLICT'));
			return null;
		}
	}
	
	public static function createTestStreamPath() {		
		$plurl = $GLOBALS['bizconf']->generatePlaylist();
		if (!empty($plurl)) {
			$qstring = $GLOBALS['bizconf']->getrsa256qstring();
			
			return "$plurl?$qstring";
		}
		else {
			Logger::writelog(array('result_code' => 431, 'result' => 'LIVE_MAPPING_CONFLICT'));
			return null;
		}
	}
}