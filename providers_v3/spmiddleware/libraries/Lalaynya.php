<?php
class Lalaynya {
	private $redisClient; private $ingroupcdns; private $ccu; private $cdnname; private $return;
	
	public function findCDN ($group, $bypass = false) {
		$host = "10.18.19.157";
		$port = 6383;
        try {
            $redis = new Redis();
            $redis->connect($host, $port);
            $redis->select($group);
            $this->ingroupcdns = $redis->keys("*");
            $redis->select(499);
            $statuscdns = $redis->mget($this->ingroupcdns);
            array_walk($statuscdns, function($item, $key) use($statuscdns) {
                $status = explode("|", $item);
                if (count($status) == 1) {
                    if ($status[0] != "RUNNING") unset($this->ingroupcdns[$key]);
                } else {
                    if ($status[0] != "RUNNING" || abs(time() - $status[1]) > 90) unset($this->ingroupcdns[$key]);
                }
            });

            if (count($this->ingroupcdns) > 0) {
                $redis->select(0);
                $ccucdns = $redis->mget($this->ingroupcdns);
                $redis->select(500);
                $maxccucdns = $redis->mget($this->ingroupcdns);
                foreach($ccucdns as $i => $ccu) {
                    if ($maxccucdns[$i] > $ccu || $bypass) $availableccus[$i] = $maxccucdns[$i] - $ccu;
                }
                $cdnmember = count($availableccus);
                if ($cdnmember > 0) {
                    ### High Rank Random ###
                    arsort($availableccus, SORT_NUMERIC);
                    $minindex = array_keys(array_slice($availableccus, 0, ceil(0.5 * $cdnmember), true));
                    $this->return = $this->ingroupcdns[$minindex[array_rand($minindex)]];
                    ### Power of 2 ###
                    // $indexes = array_rand($availableccus, 2);
                    // $minindex = $availableccus[$indexes[0]] <= @$availableccus[@$indexes[1]] ? $indexes[0] : $indexes[1];
                    // $this->return = $this->ingroupcdns[$minindex];
                } else {
                    $this->return = 406;
                }
            } else {
                $this->return = 306;
            }
            $redis->close();
        } catch (Exception $e) {
            file_put_contents(LOGDIR. "/Lalaynya.err", date("Y/m/d_H:i:s") ."  ". $e->getMessage() ."\n", FILE_APPEND);
        }
		return $this->return;
	}
}