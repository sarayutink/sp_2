<?
class Curlcstm {
	public static function establishServer ($reqUrl) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $reqUrl);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_NOSIGNAL, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT_MS, 5000);
		curl_setopt($curl, CURLOPT_USERAGENT, "PPM_Streaming_Provider");
		$result = curl_exec($curl);
		curl_close($curl);
		
		if($result !== false) {
			return $result;
		}
		else return false;
	}
	
	public static function postJsonObj ($url, $json) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: text/json',
				'Content-Length: ' . strlen($json))
		);
		$data = curl_exec($ch);
		$return = (!curl_errno($ch)) ? $data : false;
		curl_close($ch);
		return $return;
	}
}