<?
class Opensslcryption {
	public static function encrypt ($strtoencrypt) {
		$iv = openssl_random_pseudo_bytes(16);
		$encryption_key = 'TVS-PPM-01067541';

		$data = openssl_encrypt($strtoencrypt, 'aes-128-cbc', $encryption_key, OPENSSL_RAW_DATA , $iv);
		
		return urlencode(base64_encode($data ."::". $iv));
	}
	
	public static function encryptbypass () {
		$token_arr = array('aDR81TY2wbl5G3bD', 'o54YDHDpHOLxlDl0', 'mTPiMyHNGwGIjEqY', 'EjixkniGQJ51ufjD', 'gUdwhojxwe5MrdbO');
		
		$iv = openssl_random_pseudo_bytes(16);
		$encryption_key = 'TVS-PPM-01067541';

		$data = openssl_encrypt($token_arr[rand(0,count($token_arr)-1)], 'aes-128-cbc', $encryption_key, OPENSSL_RAW_DATA , $iv);
		
		return urlencode(str_replace("=", "", base64_encode($data ."::". $iv)));
	}
	
	public static function encryptstr ($strtoencrypt) {
		$iv = 'B7B4372CDFBCB3D9';
		$encryption_key = 'TVS-PPM-01067541';
		$data = openssl_encrypt($strtoencrypt, 'aes-128-cbc', $encryption_key, true , $iv);
		
		return urlencode(base64_encode($data));
	}
	
	public static function decrypt ($strtodecrypt, $key, $vector) {
		// echo "$strtodecrypt, $key, $vector";
		$strtodecrypt = base64_decode($strtodecrypt);
		return openssl_decrypt($strtodecrypt, 'aes-128-cbc', $key, OPENSSL_RAW_DATA , $vector);
		// return openssl_decrypt(base64_decode($strtodecrypt), 'aes-128-cbc', "O8CT60S7O8WEXZ0P", OPENSSL_RAW_DATA , "dOth4dKX0OlbOwT5");
	}
}