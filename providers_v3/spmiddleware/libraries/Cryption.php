<?
class Cryption {
	## MCrypt
	const key = 'TVS-PPM-01067541'; #Same as in JAVA
	const iv = 'B22C164DD885DA22'; #Same as in JAVA
 
	private static function padString($source) {
		$paddingChar = ' ';
		$size = 16;
		$x = strlen($source) % $size;
		$padLength = $size - $x;
 
		for ($i = 0; $i < $padLength; $i++) {
			$source .= $paddingChar;
		}
 
		return $source;
	}
	
	public static function encrypt($str) {
		$td = mcrypt_module_open('rijndael-128', '', 'cbc', self::iv);
 
		mcrypt_generic_init($td, self::key, self::iv);
		$str = self::padString($str);
		$encrypted = mcrypt_generic($td, $str);
 
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		return urlencode(str_replace("=", "", base64_encode($encrypted)));
	}
	
	public static function encryptBypass ($appid) {
		return true;
	}
}

// var_dump(Cryption::encrypt(time()));