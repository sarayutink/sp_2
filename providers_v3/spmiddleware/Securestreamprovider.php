<?
require_once 'Securestreamprovider.req';

class Securestreamprovider {
	public static function provide ($request, $response) {
		$body = $request->getBody();
		$GLOBALS['ctrl_name'] = "Securestreamprovider";
		$GLOBALS['lbl_name'] = "lalaynya";
		$GLOBALS['json_object'] = json_decode($body);
		unset($body);
		
		## validate post json
		if (Variable::validate()) {
			require_once($GLOBALS['src_dir'] ."/controllers/Licensecontrol.php");
			if (Licensecontrol::authenticate() == 200) {
				$apconf = ucfirst($GLOBALS['json_object']->appid)."config";
				require_once($GLOBALS['src_dir'] ."/configures/streamingprovider/".$apconf.".php");
				$GLOBALS['bizconf'] = new $apconf();
				## check channel
				if ($GLOBALS['bizconf']->isValidChannelID()) {
					## check ccu control
					require_once($GLOBALS['src_dir'] ."/controllers/Ccucontrol.php");
					if (Ccucontrol::check() == 200) {
						## get streaming server via load balancer
						require_once($GLOBALS['src_dir'] ."/controllers/Loadbalancecontrol.php");
						$server = Loadbalancecontrol::findServer();
						if (intval($server) == 0) {
							require_once($GLOBALS['src_dir'] ."/controllers/Streamcontrol.php");
							$action = Streamcontrol::createStreamPath();
							if(!is_null($action)) {
								$return = array('result_code' => 200, 'result' => "http://".$server.$action);
								//Logger::writelog(array('result_code' => 200, 'result' => "http://".$server.$action));
							}
							else $return = array('result_code' => 430, 'result' => "Cannot find playlist.");
						}
						// elseif ($server == "406") $return = array('result_code' => 200, 'result' => "406");
						else $return = array('result_code' => $return, 'result' => "Cannot find streaming server.");
					}
					else $return = array('result_code' => 410, 'result' => "Your concurrent reaches limit.");
				}
				else $return = array('result_code' => 610, 'result' => "Cannot find channel to streaming.");
			}
			else $return = array('result_code' => 620, 'result' => "Invalid Token.");
		}
		else $return = array('result_code' => 600, 'result' => "Invalid request.");
		
		Logger::writelog($return);
		
		return $return;
    }
}