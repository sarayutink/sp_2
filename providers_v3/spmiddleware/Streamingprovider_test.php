<?
require_once 'Streamingprovider_test.req';

class Streamingprovider {
	public static function provide ($request, $response) {
		$body = $request->getBody();
		$GLOBALS['ctrl_name'] = "Streamingprovider";
		$GLOBALS['lbl_name'] = "lalaynya";
		$GLOBALS['json_object'] = json_decode($body);
		unset($body);
		
		## validate post json
		if (Variable::validate()) {
			$apconf = ucfirst($GLOBALS['json_object']->appid)."testconfig";
			require_once($GLOBALS['src_dir'] ."/configures/streamingprovider/".$apconf.".php");
			$GLOBALS['bizconf'] = new $apconf();
			## check channel
			if ($GLOBALS['bizconf']->isValidChannelID()) {
				## check ccu control
				require_once($GLOBALS['src_dir'] ."/controllers/Ccucontrol_test.php");
				// var_dump(Ccucontrol_test::check();
				// var_dump(Ccucontrol_test::check());
				if (Ccucontrol_test::check() == 200) {
					// ## get streaming server via load balancer
					require_once($GLOBALS['src_dir'] ."/controllers/Loadbalancecontrol.php");
					// $server = Loadbalancecontrol::findServer("http://server_loadbalan:8080/agentB.php");
					$server = Loadbalancecontrol::findServer();
					// if (!is_null($server) && $server != "406") {
						// var_dump($server);
					if (strrpos($server, "truevisions.tv")) {
						require_once($GLOBALS['src_dir'] ."/controllers/Streamcontrol.php");
						$action = Streamcontrol::createStreamPath();
						if(!is_null($action)) {
							$return = array('result_code' => 200, 'result' => "http://".$server.$action);
							Logger::writelog(array('result_code' => 200, 'result' => "http://".$server.$action));
						}
						else $return = array('result_code' => 430, 'result' => "Cannot find playlist.");
					}
					elseif ($server == "406") $return = array('result_code' => 200, 'result' => "406");
					else $return = array('result_code' => 420, 'result' => "Cannot find streaming server.");
				}
				else $return = array('result_code' => 410, 'result' => "Your concurrent reaches limit.");
			}
			else $return = array('result_code' => 610, 'result' => "Cannot find channel to streaming.");
		}
		else $return = array('result_code' => 600, 'result' => "Invalid request.");
		
		
		return $return;
    }
}