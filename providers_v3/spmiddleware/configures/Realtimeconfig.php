<?
class Realtimeconfig {
	var $bizconf;
	
	public function __construct () {
		$this->bizconf = $this->loadLocalXML($GLOBALS['conf_path']);
	}
	
	private function loadLocalXML ($filepath) {
		if (file_exists($filepath)) {
			/** load config in XML */
			try {
				$xml_obj = simplexml_load_file($filepath);
				return $xml_obj->{$GLOBALS['json_object']->appid};
			}
			catch(Exception $e) {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	public function generatePlaylist () {
		$return = $this->bizconf->appinst ."/". $GLOBALS['json_object']->channelid . $this->bizconf->stream;
		// if ($GLOBALS['json_object']->type == "live") $return = $this->bizconf->appinst ."/". $GLOBALS['json_object']->channelid . "test_a1.stream";
		return $return;
	}
	
	public function getGroupId () {
		$return = $this->bizconf->group;
		return (string)$return;
	}
	
	public function getrsaqstring () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		//require_once $GLOBALS['src_dir'] .'/libraries/ArsOpensslcryption.php';
		$channelid = ($GLOBALS['json_object']->channelid != '021') ? $GLOBALS['json_object']->channelid : '135';
		$ch_list = "o021;o022;179";
		// $encrypt = Opensslcryption::encryptstr(time() ."-ppmps");
		$encrypt = Opensslcryption::encryptstr(time() ."-tvsrtsp");
		//$encrypt = Arsopensslcryption::encryptstr(time() ."-tvsrtsp");
		@$querystring .= "appid={$GLOBALS['json_object']->appid}&mpass={$encrypt}";
		
		return $querystring;
	}
}