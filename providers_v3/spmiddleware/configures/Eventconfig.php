<?
class Eventconfig {
	public $bizconf;
	
	public function __construct () {
		$this->bizconf = $this->loadRedisXML();
	}
	
	private function loadRedisXML () {
		require_once __DIR__ ."/Configdistributor.php";
		$conf_dist = new Configdistributor();
		$ctrl_name = strtolower($GLOBALS['ctrl_name']);
		$type = (strtolower($GLOBALS['json_object']->programstate) == "timeshift" && $ctrl_name == "eventstreamprovider" ? "live" : $GLOBALS['json_object']->programstate);
		// var_dump($conf_dist->distributeConfig($ctrl_name, strtolower($GLOBALS['json_object']->programid), $type));
		// return true;
		return $conf_dist->distributeConfig($ctrl_name, strtolower($GLOBALS['json_object']->programid), $type);
	}
	
	public function verifyApplicationCCU ($ccucount = false) {
		if ($this->bizconf->application->ccu == 'unlimit') return true;
		else return $ccucount < $this->bizconf->application->ccu;
	}
	
	public function verifyUserCCU ($ccucount = false) {
		if ($this->bizconf->user->ccu == 'unlimit') return true;
		else return $ccucount < $this->bizconf->user->ccu;
	}
	
	public function generatePlaylist () {
		$object = $GLOBALS['json_object'];
		switch (strtolower($object->programstate)) {
			case "live" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. strtolower($object->streamlvl) .'" and @device="'. strtolower($object->visitor) .'" and @type="'. strtolower($object->programstate).'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return "/".(string)$ret_arr[0]->appinst."/smil:".(string)$ret_arr[0]->smil[0]."/playlist.m3u8";
				else return null;
			break;
			case "timeshift" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="live"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. $object->streamlvl .'" and @device="'. $object->visitor .'" and @type="'. strtolower($object->programstate) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return "/".(string)$ret_arr[0]->appinst."/smil:".(string)$ret_arr[0]->smil[0]."/playlist.m3u8";
				else return null;
			break;
			case "catchup" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. strtolower($object->streamlvl) .'" and @device="'. strtolower($object->visitor) .'" and @type="'. strtolower($object->programstate) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) {
					$smil = str_replace("yyyymmdd", date("Ymd", (string)$ret_arr[0]->stime), (string)$ret_arr[0]->smil[0]);
					return "/".(string)$ret_arr[0]->appinst."/". $smil ."/playlist.m3u8";
				}
				else return null;
			break;
			case "vod" :
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) {
					$path  = explode("/", parse_url((string)$ret_arr[0]->override_url)['path']);
					$path[1] = "vodevent";
					// var_dump($path);
					return implode("/", $path);
				} 
				else return null;
			break;
			default :
				$query = '//program[@id="'. ($object->programid).'"]/state[@id="'. $object->programstate .'"]/streamprofile/language[@id="'.$object->langid.'"]/profile[@streamlevel="'.$object->streamlvl.'" and @device="'.$object->visitor.'" and @type="'. strtolower($object->programstate).'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return "/".(string)$ret_arr[0]->appinst."/smil:".(string)$ret_arr[0]->smil[0]."/playlist.m3u8";
				else return null;
			break;
		}
		// if ($object->type == "live") return "/".$ret_arr->live->appinst."/".$ret_arr->live->smil."/playlist.m3u8";
		// else if ($object->type == "timeshift") return "/".$ret_arr->timeshift->appinst."/".$ret_arr->timeshift->smil."/playlist.m3u8";
		// else return "/".$ret_arr->catchup->appinst."/".$ret_arr->catchup->smil."{$this->changeDateFormat($object->stime, "Ymd")}/playlist.m3u8";
	}
	
	public function getStreamname () {
		$object = $GLOBALS['json_object'];
		switch (strtolower($object->programstate)) {
			case "live" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. strtolower($object->streamlvl) .'" and @device="'. strtolower($object->visitor) .'" and @type="'. strtolower($object->programstate).'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return explode("_", (string)$ret_arr[0]->smil[0])[0];
				else return null;
			break;
			case "timeshift" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="live"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. $object->streamlvl .'" and @device="'. $object->visitor .'" and @type="'. strtolower($object->programstate) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return explode("_", (string)$ret_arr[0]->smil[0])[0];
				else return null;
			break;
			case "catchup" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. strtolower($object->streamlvl) .'" and @device="'. strtolower($object->visitor) .'" and @type="'. strtolower($object->programstate) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) {
					$smil = str_replace("yyyymmdd", date("Ymd", (string)$ret_arr[0]->stime), (string)$ret_arr[0]->smil[0]);
					return explode("_", $smil)[0];
				}
				else return null;
			break;
			case "vod" :
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) {
					$path  = explode("/", parse_url((string)$ret_arr[0]->override_url)['path']);
					// echo substr(explode("_", $path[2])[0], 4);
					// $path[1] = "vodevent";
					// var_dump($path);
					return substr(explode("_", $path[2])[0], 5);
				} 
				else return null;
			break;
			default :
				$query = '//program[@id="'. ($object->programid).'"]/state[@id="'. $object->programstate .'"]/streamprofile/language[@id="'.$object->langid.'"]/profile[@streamlevel="'.$object->streamlvl.'" and @device="'.$object->visitor.'" and @type="'. strtolower($object->programstate).'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return explode("_", (string)$ret_arr[0]->smil[0])[0];
				else return null;
			break;
		}
	}
	
	public function getGroupId () {
		$object = $GLOBALS['json_object'];
		switch (strtolower($object->programstate)) {
			case "live" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. $object->streamlvl .'" and @device="'. $object->visitor .'" and @type="'. strtolower($object->programstate) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return (string)$ret_arr[0]->group;
				else return null;
			break;
			case "timeshift" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="live"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. $object->streamlvl .'" and @device="'. $object->visitor .'" and @type="'. strtolower($object->programstate) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return (string)$ret_arr[0]->group;
				else return null;
			break;
			case "catchup" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. $object->streamlvl .'" and @device="'. $object->visitor .'" and @type="'.strtolower($object->programstate).'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return (string)$ret_arr[0]->group;
				else return null;
			break;
			case "vod" :
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) {
					$host  = parse_url((string)$ret_arr[0]->override_url)['host'];
					preg_match_all('/\d+/', $host, $matches);
					return $matches[0][0];
				}
				else return null;
				// $result = Curlcstm::establishServer("http://ottstreamingprovider.ubc.co.th/StreamProvide/getStreamByProgramID?program_id={$object->programid}&profile_name=vod");
				// var_dump($result);
				// $arr = json_decode($result, true);
				// var_dump($arr["language"][$object->langid]);
			break;
			default :
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. $object->streamlvl .'" and @device="'. $object->visitor .'" and @type="'. strtolower($object->programstate) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return "/".(string)$ret_arr[0]->appinst."/smil:".(string)$ret_arr[0]->smil[0]."/playlist.m3u8";
				else return null;
			break;
		}
		
	}
	
	public function changeDateFormat ($dateformat, $fromat) {
		return date($fromat, $dateformat);
	}
	
	public function getrsaqstring () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		//require_once $GLOBALS['src_dir'] .'/libraries/ArsOpensslcryption.php';
		$token_arr = array('oqxg55CptqALr0','aFEacfYcpOtkGi','famnCWFwjvlksk','5M1L5wcBblA0IX','oP9Qlqas8YJlvp','9UxURr9saLEd5F');
		$streamname = $this->getStreamname();
		$txt2dcrp = time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $streamname ."|". $GLOBALS['json_object']->uid ."|unlimit";
		$encrypt = Opensslcryption::encrypt($txt2dcrp);
		//$encrypt = Arsopensslcryption::encrypt($txt2dcrp);
		// $encrypt = Opensslcryption::encryptbypass();
		if ($GLOBALS['json_object']->type == "timeshift") @$querystring = "dvr=&";
		elseif ($GLOBALS['json_object']->type == "catchup") @$querystring = "dvr=&wowzadvrplayliststart={$this->changeDateFormat($GLOBALS['json_object']->stime, "Y-m-d-H:i:s")}&wowzadvrplaylistduration={$GLOBALS['json_object']->duration}&";
		@$querystring .= "appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}";
		
		return $querystring;
	}
}