<?
require_once($GLOBALS['src_dir'] ."/configures/Streamconfig.php");
class Anywhereconfig extends Streamconfig {
	public function __construct () {
		parent::__construct();
		// var_dump($this);
	}
	
	public function generatePlaylist ($default = "th") {
		// echo $object->programstate;
		$visitor = ($GLOBALS['json_object']->visitor == "web" ? "mobile" : $GLOBALS['json_object']->visitor);
		$query = '//streamprofile/language[@id="'. strtolower($GLOBALS['json_object']->langid) .'"]/profile[@streamlevel="'. strtolower($GLOBALS['json_object']->streamlvl) .'" and @device="'. strtolower($visitor) .'" and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
		$ret_arr = $this->bizctrl->xpath($query);
		if (count($ret_arr) > 0) {
			// var_dump($object);
			$smil = str_replace("yyyymmdd", date("Ymd", (int)$GLOBALS['json_object']->stime), (string)$ret_arr[0]->smil[0]);
			return "/".(string)$ret_arr[0]->appinst."/". $smil ."/playlist.m3u8";
		}
		else {
			$query = '//streamprofile/language[@id="'. strtolower($default) .'"]/profile[@streamlevel="'. strtolower($GLOBALS['json_object']->streamlvl) .'" and @device="'. strtolower($visitor) .'" and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
			$ret_arr = $this->bizctrl->xpath($query);
			if (count($ret_arr) > 0) {
				$smil = str_replace("yyyymmdd", date("Ymd", (int)$GLOBALS['json_object']->stime), (string)$ret_arr[0]->smil[0]);
				return "/".(string)$ret_arr[0]->appinst."/". $smil ."/playlist.m3u8";
			}
			else return null;
		}
	}
	
	public function getGroupId ($default = "th") {
		$visitor = ($GLOBALS['json_object']->visitor == "web" ? "mobile" : $GLOBALS['json_object']->visitor);
		$query = '//streamprofile/language[@id="'. strtolower($GLOBALS['json_object']->langid) .'"]/profile[@streamlevel="'. $GLOBALS['json_object']->streamlvl .'" and @device="'. $visitor .'" and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
		$ret_arr = $this->bizctrl->xpath($query);
		// var_dump($query);
		if (count($ret_arr) > 0) return (string)$ret_arr[0]->group;
		else {
			$query = '//streamprofile/language[@id="'. strtolower($default) .'"]/profile[@streamlevel="'. $GLOBALS['json_object']->streamlvl .'" and @device="'. $visitor .'" and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
			$ret_arr = $this->bizctrl->xpath($query);
			if (count($ret_arr) > 0) return (string)$ret_arr[0]->group;
			else return null;
		}
	}
	
	
	// public function getGroupId ($object) {
		// $query = '//channel[@id="'.$object->channelid.'"]/language[@id="'.$object->langid.'"]/profile[@streamlevel="'.$object->streamlvl.'" and @device="'.$object->visitor.'" and @type="'.$object->type.'"]';
		// $ret_arr = $this->bizunit->xpath($query);
		// if (count($ret_arr) > 0) {
			// return isset($object->charge) ? ($object->charge == true ?  (string)$ret_arr[0]->charge :  (string)$ret_arr[0]->group) :  (string)$ret_arr[0]->group;
		// }
		// else return null;
	// }
}
?>