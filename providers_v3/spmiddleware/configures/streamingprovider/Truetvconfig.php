<?
include_once( dirname(__DIR__). '/Bizconfig.php');
class Truetvconfig extends Bizconfig {
	var $bizunit;
	
	public function __construct ($array = false) {
		parent::__construct($array);
	}
	
	public function getGroupId ($object) {
		$query = '//channel[@id="'.$object->channelid.'"]/language[@id="'.$object->langid.'"]/profile[@streamlevel="'.$object->streamlvl.'" and @device="'.$object->visitor.'" and @type="'.$object->type.'"]';
		$ret_arr = $this->bizunit->xpath($query);
		if (count($ret_arr) > 0) {
			return isset($object->charge) ? ($object->charge == true ?  (string)$ret_arr[0]->charge :  (string)$ret_arr[0]->group) :  (string)$ret_arr[0]->group;
		}
		else return null;
	}
}
?>