<?
include_once(dirname(__DIR__). '/Streamconfig.php');
class Tvtruelifeconfig extends Streamconfig {
	var $bizunit;
	
	public function __construct ($array = false) {
		parent::__construct($array['appid'], $array['channelid']);
	}
	
	public function getGroupId () {
		$conf_path = "/www/webapps/providers_v3_static_group.xml";
		$group_conf = simplexml_load_file($conf_path);
		$group = $group_conf->{$GLOBALS['json_object']->appid}->{$GLOBALS['json_object']->type};
		if (!is_null($group)) return $group;
		else return parent::getGroupId();
	}
}
?>