<?
class Configdistributor {
	private $json_obj;
	private $obms_api_url = "http://ottstreamingprovider.ubc.co.th/StreamProvide/getStreamByProgramID";
	private $redis_expire = 60;
	private $prog_db = 1;
	private $type_arr = array('live' => "live", 'ts' => "timeshift", 'cu' => "catchup");
	private $device_arr = array('m' => "mobile", 's' => "stb");
	
	public function distributeConfig ($ctrl_name, $programid, $type) {
		$cachestatus = $this->getCachingStatus($programid, $type);
		// echo "cachestatus =>\n";
		// var_dump($cachestatus);
		// echo "\n";
		if ($cachestatus === false) {
			$this->setCachingStatus($programid, $type);
			// echo "setCachingStatus :: $programid , $type";
			// echo "\n";
			
			$this->generateProviderConfig($ctrl_name, $programid);
			// echo "generateProviderConfig :: $programid";
			// echo "\n";
			// var_dump(simplexml_load_string($this->loadConfig ($programid, $type)));
			return simplexml_load_string($this->loadConfig ($programid, $type));
		}
		elseif ($cachestatus == "WAIT") {
			sleep(1);
			return ($this->distributeConfig ($ctrl_name, $programid, $type));
		}
		else {
			// var_dump(simplexml_load_string($cachestatus));
			return simplexml_load_string($cachestatus);
		}
		// return true;
	}
	
	public function getStreamFromJson ($get_param) {
		require_once dirname(__DIR__).'/libraries/Curlcstm.php';
		$result = Curlcstm::establishServer($this->obms_api_url. "?program_id={$get_param['program_id']}&profile_name={$get_param['profile_id']}");
		return $result;
	}
	
	public function getCachingStatus ($progid, $type) {
		return $GLOBALS['redis']->getRedis(strtolower($progid) ."_". strtolower($type), $this->prog_db);
	}
	
	public function setCachingStatus ($progid, $type, $offset = null) {
		$GLOBALS['redis']->setRedis(strtolower($progid) ."_". strtolower($type), "WAIT", $this->prog_db, $this->redis_expire);
	}
	
	public function separateProfile ($streamlevel, $catchup = false) {
		$tmp_arr = explode("_", $streamlevel);
		if (!$catchup) {
			if (count($tmp_arr) > 1) {
				$ret_arr['streamlevel'] = $tmp_arr[0];
				$ret_arr['type'] = $this->type_arr[$tmp_arr[1]];
				$ret_arr['device'] = $this->device_arr[$tmp_arr[2]];
				return $ret_arr;
			}
			else return false;
		}
		else {
			if (count($tmp_arr) > 1) {
				$ret_arr['streamlevel'] = $tmp_arr[0];
				$ret_arr['type'] = $this->type_arr[$tmp_arr[1]];
				$ret_arr['device'] = $this->device_arr[$tmp_arr[2]];
				return $ret_arr;
			}
			else return false;
		}
	}
	
	public function transformJson () {
		if ($this->json_obj->status == 200) {
			switch (strtoupper($this->json_obj->state)) {
				case "VOD" :
					foreach ($this->json_obj->language as $lang => $lang_obj) {
						foreach ($lang_obj as $row) {
							$xml_str = '<override_url>'. @$row->override_url .'</override_url>';
							$xml_str = '<language id="'. strtolower($lang) .'">'. @$xml_str .'</language>';
						}
					}
					$xml_str = '<streamprofile>'. $xml_str .'</streamprofile>';
					$xml_str = '<state  id="'. strtolower($this->json_obj->state) .'">'. $xml_str .'</state>';
					$xml_str = '<program  id="'. strtolower($this->json_obj->program_id) .'">'. $xml_str .'</program>';
					
					return $xml_str;
				break;
				case "LIVE" :
					foreach ($this->json_obj->language as $lang => $lang_obj) {
						$chk_dup_arr = array();
						$profile_str = "";
						foreach ($lang_obj as $row) {
							$stream_lvl_arr = $this->separateProfile($row->stream_level);
							if ($stream_lvl_arr <> false && !in_array($stream_lvl_arr, $chk_dup_arr)) {
								$chk_dup_arr[] = $stream_lvl_arr;
								$profile_child_str = '<smil>'. $row->smil .'</smil>';
								$profile_child_str .= '<appinst>'. $row->app .'</appinst>';
								$profile_child_str .= '<group>'. $row->server_group .'</group>';
								$profile_str .= '<profile streamlevel="'. strtolower($stream_lvl_arr['streamlevel']) .'" device="'. strtolower($stream_lvl_arr['device']) .'" type="'. strtolower($stream_lvl_arr['type']) .'">'. $profile_child_str .'</profile>';
							}
						}
						@$xml_str .= '<language id="'. strtolower($lang) .'">'. $profile_str .'</language>';
					}
					$xml_str = '<streamprofile>'. $xml_str .'</streamprofile>';
					$xml_str = '<state  id="'. strtolower($this->json_obj->state) .'">'. $xml_str .'</state>';
					$xml_str = '<program  id="'. strtolower($this->json_obj->program_id) .'">'. $xml_str .'</program>';
					
					return $xml_str;
				break;
				case "CATCHUP" :
					foreach ($this->json_obj->language as $lang => $lang_obj) {
						$chk_dup_arr = array();
						$profile_str = "";
						foreach ($lang_obj as $row) {
							$stream_lvl_arr = $this->separateProfile($row->stream_level, true);
							// var_dump($stream_lvl_arr);
							if ($stream_lvl_arr <> false && !in_array($stream_lvl_arr, $chk_dup_arr)) {
								$chk_dup_arr[] = $stream_lvl_arr;
								$start = DateTime::createFromFormat('!Y-m-d H:i:s', $this->json_obj->content_start_date)->getTimestamp();
								$end = DateTime::createFromFormat('!Y-m-d H:i:s', $this->json_obj->content_end_date)->getTimestamp() - $start;
								$profile_child_str = '<smil>'. $row->smil .'</smil>';
								$profile_child_str .= '<appinst>'. $row->app .'</appinst>';
								$profile_child_str .= '<group>'. $row->server_group .'</group>';
								$profile_child_str .= '<stime>'. $start .'</stime>';
								$profile_child_str .= '<duration>'. $end .'000</duration>';
								$profile_str .= '<profile streamlevel="'. strtolower($stream_lvl_arr['streamlevel']) .'" device="'. strtolower($stream_lvl_arr['device']) .'" type="'. strtolower($stream_lvl_arr['type']) .'">'. $profile_child_str .'</profile>';
							}
						}
						@$xml_str .= '<language id="'. strtolower($lang) .'">'. $profile_str .'</language>';
					}
					$xml_str = '<streamprofile>'. $xml_str .'</streamprofile>';
					$xml_str = '<state  id="'. strtolower($this->json_obj->state) .'">'. $xml_str .'</state>';
					$xml_str = '<program  id="'. strtolower($this->json_obj->program_id) .'">'. $xml_str .'</program>';
					
					return $xml_str;
				break;
			}
		}
		else {
			return null;
		}
	}
	
	public function setConfig2Redis ($xml) {
		$GLOBALS['redis']->setRedis(strtolower($this->json_obj->program_id) ."_". strtolower($this->json_obj->state), $xml, $this->prog_db, $this->redis_expire);
	}
	
	public function generateProviderConfig ($ctrl_name, $programid) {
		$json = $this->getStreamFromJson(array('ctrl_name' => $ctrl_name, 'program_id' => $programid, 'profile_id' => "liveevent"));
		try {
			$this->json_obj = json_decode($json);
			$xml = $this->transformJson();
			$this->setConfig2Redis($xml);
		}
		catch( Exception $e ) {
			echo "Caught exception : <b>".$e->getMessage()."</b><br/>";
		}
		// var_dump($xml);
	}
	
	public function loadConfig ($programid, $type) {
		$xml_str = "<?xml version='1.0' encoding='UTF-8'?><configure>";
		$xml_str .= '<streammapping>'. $GLOBALS['redis']->getRedis("{$programid}_{$type}", $this->prog_db) .'</streammapping>';
		$xml_str .= '</configure>';
		return $xml_str;
	}
}

// $confdis = new Configdistributor("liveevent");
// $json_obj = json_decode($confdis->getStreamFromJson(array('ctrl_name' => "streamingprovider", 'channel_code' => "207", 'profile_id' => "SP")));
// var_dump($confdis);
// $res = ($confdis->distributeConfig("streamingprovider", "207", "SP"));
// var_dump($res);
// foreach ($json_obj->language as $lang => $lang_obj) {
// 	if ($lang == "th") {
//		echo $lang."\n";
// 		$chk_dup_arr = array();
// 		$profile_str = "";
// 		var_dump($chk_dup_arr);
// 		foreach ($lang_obj as $row) {
// 			var_dump($chk_dup_arr);
// 			$stream_lvl_arr = $confdis->separateProfile($row->stream_level);
// 			var_dump($stream_lvl_arr);
// 			if ($stream_lvl_arr <> false && !in_array($stream_lvl_arr, $chk_dup_arr)) {
// 				$chk_dup_arr[] = $stream_lvl_arr;
// 				$profile_child_str = '<smil>'. $row->smil .'</smil>';
// 				$profile_child_str .= '<appinst>'. $row->app .'</appinst>';
// 				$profile_child_str .= '<group>'. $row->server_group .'</group>';
// 				$profile_str .= '<profile streamlevel="'. $stream_lvl_arr['streamlevel'] .'" device="'. $stream_lvl_arr['device'] .'" type="'. $stream_lvl_arr['type'] .'">'. $profile_child_str .'</profile>';
// 			}
// 		}
// 		@$xml_str .= '<language id="'. $lang .'">'. $profile_str .'</language>';
// 	}
// }
// $xml_str = '<streamprofile>'. $xml_str .'</streamprofile>';
// $xml_str = '<state  id="LIVE">'. $xml_str .'</state>';
// $xml_str = '<program  id="TVP201602000000">'. $xml_str .'</program>';

// echo $xml_str;
// $str = ($confdis->distributeConfig('eventstreamprovider', 'TVP201602000111', 'CATCHUP'));
// var_dump($str);

// $str = simplexml_load_string($str);
// var_dump($str);