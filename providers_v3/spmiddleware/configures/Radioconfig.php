<?
class Radioconfig {
	var $bizconf;
	
	public function __construct () {
		$this->bizconf = $this->loadLocalXML($GLOBALS['src_dir'] .'/assets/radiostreamprovider/'. $GLOBALS['json_object']->appid .'.conf');
	}
	
	private function loadLocalXML ($filepath) {
		if (file_exists($filepath)) {
			/** load config in XML */
			try {
				$xml_obj = simplexml_load_file($filepath);
				return $xml_obj;
			}
			catch(Exception $e) {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	public function generatePlaylist () {
		$cols = explode(".", $GLOBALS['json_object']->channelid);
		$return = $this->bizconf->appinst ."/". $cols[0] .".stream_aac";
		return ($GLOBALS['json_object']->visitor == "wap") ? $return : $return ."/playlist.m3u8";
	}
	
	public function getGroupId () {
		$return = $this->bizconf->group->{$GLOBALS['json_object']->visitor};
		return (string)$return;
	}
	
	public function getrsaqstring () {
		//require_once $GLOBALS['src_dir'] .'/libraries/ArsOpensslcryption.php';
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$token_arr = array('aDR81TY2wbl5G3bD', 'o54YDHDpHOLxlDl0', 'mTPiMyHNGwGIjEqY', 'EjixkniGQJ51ufjD', 'gUdwhojxwe5MrdbO', 'wGpILA7FZihFnVBC');

		$stream = substr($GLOBALS['json_object']->channelid, 0, strpos($GLOBALS['json_object']->channelid, "_"));

		$encrypt = Opensslcryption::encrypt(time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|0.0.0.0|". $stream ."|". $GLOBALS['json_object']->uid ."|". @$dvr);
		//$encrypt = Arsopensslcryption::encrypt(time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|0.0.0.0|". $stream ."|". $GLOBALS['json_object']->uid ."|". @$dvr);
		// $encrypt = Opensslcryption::encrypt($token_arr);

		$querystring .= "appid={$GLOBALS['json_object']->appid}&visitor={$GLOBALS['json_object']->visitor}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}";
		
		return $querystring;
	}
}