<?
class Tvodconfig {
	var $bizconf;
	
	public function __construct () {
		$this->bizconf = $this->loadBizConf($GLOBALS['json_object']->appid.".conf", $GLOBALS['src_dir'] ."/assets/". strtolower($GLOBALS['ctrl_name']) ."/");
	}
	
	public function loadBizConf ($appname, $path = APPPATH) {
		if (file_exists($path.$appname)) {
			/** load config in JSON */
			try {
				return json_decode(file_get_contents($path.$appname));
			}
			catch(Exception $e) {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	// public function generatePlaylist () {
		// return "/". $this->bizconf->streammapping->appinst ."/". $GLOBALS['json_object']->streamname. "/playlits.m3u8";
	// }
	
	public function generateManifest () {
		switch ($GLOBALS['json_object']->drm) {
			case "wv":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Wvconfig.php";
				return Wvconfig::generateManifest();
			break;
			case "fp":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Fpconfig.php";
				return Fpconfig::generateManifest();
			break;
			default:
				return null;
			break;
		}
	}
	
	public function getBalencer () {
		return "http://server_loadbalan:8080/lalaynya.php";
	}
	
	public function getGroupId () {
		switch ($GLOBALS['json_object']->drm) {
			case "wv" :
				return 106;
			break;
			case "fp" :
				return 107;
			break;
			default :
				return 105;
			break;
		}
	}
	
	// public function changeDateFormat ($dateformat, $fromat) {
		// return date($fromat, $dateformat);
	// }
	
	// public function getrsaqstring () {
		// require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		// $str2decrypt = time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". explode("_", $GLOBALS['json_object']->streamname)[0] ."|". $GLOBALS['json_object']->uid ."|unlimit";
		// $encrypt = Opensslcryption::encrypt($str2decrypt);
		// $encrypt = Opensslcryption::encryptbypass();
		// $querystring = "appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&sessionid={$GLOBALS['json_object']->sessionid}&mpass={$encrypt}&visitor={$GLOBALS['json_object']->visitor}";
		
		// return $querystring;
	// }
	
	public function getLicense () {
		switch ($GLOBALS['json_object']->drm) {
			case "wv":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Wvconfig.php";
				return Wvconfig::generateLicense();
			break;
			case "fp":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Fpconfig.php";
				return Fpconfig::generateLicense();
			break;
			default:
				return null;
			break;
		}
	}
}