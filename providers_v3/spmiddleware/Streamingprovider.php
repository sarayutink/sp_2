<?
require_once 'Streamingprovider.req';

class Streamingprovider {
	public static function provide ($request, $response) {
		$body = $request->getBody();
		$GLOBALS['ctrl_name'] = "Streamingprovider";
		$GLOBALS['lbl_name'] = "lalaynya";
		$GLOBALS['json_object'] = json_decode($body);
		unset($body);
		
		## validate post json
		$isBlacklist = Blacklist::isBlacklist();
		if (Variable::validate()) {
			$apconf = ucfirst($GLOBALS['json_object']->appid)."config";
			require_once($GLOBALS['src_dir'] ."/configures/streamingprovider/".$apconf.".php");
			$GLOBALS['bizconf'] = new $apconf();
			## check channel
			if ($GLOBALS['bizconf']->isValidChannelID()) {
				## check ccu control
				require_once($GLOBALS['src_dir'] ."/controllers/Ccucontrol.php");
				if (Ccucontrol::check() == 200) {
					## get streaming server via load balancer
					require_once($GLOBALS['src_dir'] ."/controllers/Loadbalancecontrol.php");
					## get streaming server via load balancer api
					$server = Loadbalancecontrol::findServer();
					## get streaming server via local settings
					// $server = Loadbalancecontrol::randomServer();
					if (intval($server) == 0) {
						require_once($GLOBALS['src_dir'] ."/controllers/Streamcontrol.php");
						$action = Streamcontrol::createStreamPath();
						if(!is_null($action)) {
							$protocol = "http";
							if (!in_array($GLOBALS['json_object']->channelid, explode("|", "107|en107")) && in_array($GLOBALS['json_object']->visitor, explode("|", "web|lwweb")) && in_array($GLOBALS['json_object']->type, explode("|", "live"))){
							// if (!in_array($GLOBALS['json_object']->channelid, explode("|", "107|en107")) && in_array($GLOBALS['json_object']->appid, explode("|", "trueidv2|trueselect")) && in_array($GLOBALS['json_object']->visitor, explode("|", "web|lwweb")) && in_array($GLOBALS['json_object']->type, explode("|", "live"))){
								 $protocol = "https";
							}
							if(($GLOBALS['json_object']->appid == 'trueselect') && ($GLOBALS['json_object']->type == 'catchup')){
								$protocol = "https";
							}
							$return = array('result_code' => 200, 'result' => "$protocol://".$server.$action);
							Logger::writelog(array('result_code' => 200, 'result' => "$protocol://".$server.$action));
						}
						else $return = array('result_code' => 430, 'result' => "Cannot find playlist.");
					}
					// elseif ($server == "106") $return = array('result_code' => 200, 'result' => "406");
					else $return = array('result_code' => 420, 'result' => "Cannot find streaming server.");
				}
				else $return = array('result_code' => 410, 'result' => "Your concurrent reaches limit.");
			}
			else {
				Logger::writelog(array('result_code' => 610, 'result' => "Cannot find channel to streaming."));
				$return = array('result_code' => 610, 'result' => "Cannot find channel to streaming.");
			}
		}
		elseif ($isBlacklist) {
			Logger::writelog(array('result_code' => 640, 'result' => "Blacklisted."));
			$return = array('result_code' => 640, 'result' => "UID_BANNED");
		}
		else {
			Logger::writelog(array('result_code' => 600, 'result' => "Invalid request."));
			$return = array('result_code' => 600, 'result' => "Invalid request.");
		}
		
		
		return $return;
    }
}